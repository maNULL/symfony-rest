<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PreferenceRepository")
 */
class Preference
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $sortValue;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $filterValue;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TaskList", mappedBy="preferences")
     */
    private $list;

    public function __construct()
    {
        $this->list = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSortValue(): ?string
    {
        return $this->sortValue;
    }

    /**
     * @param string $sortValue
     *
     * @return Preference
     */
    public function setSortValue(string $sortValue): self
    {
        $this->sortValue = $sortValue;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilterValue(): ?string
    {
        return $this->filterValue;
    }

    /**
     * @param string $filterValue
     *
     * @return Preference
     */
    public function setFilterValue(string $filterValue): self
    {
        $this->filterValue = $filterValue;

        return $this;
    }

    /**
     * @return Collection|TaskList[]
     */
    public function getList(): Collection
    {
        return $this->list;
    }

    /**
     * @param TaskList $list
     *
     * @return Preference
     */
    public function addList(TaskList $list): self
    {
        if (!$this->list->contains($list)) {
            $this->list->add($list);
            $list->setPreferences($this);
        }

        return $this;
    }

    /**
     * @param TaskList $list
     *
     * @return Preference
     */
    public function removeList(TaskList $list): self
    {
        if ($this->list->contains($list)) {
            $this->list->removeElement($list);
            // set the owning side to null (unless already changed)
            if ($list->getPreferences() === $this) {
                $list->setPreferences(null);
            }
        }

        return $this;
    }
}
