<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskListRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TaskList
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;
    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"default": "background.png"})
     */
    private $background;
    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"default": "background.png"})
     */
    private $backgroundPath;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="list", cascade={"remove"})
     */
    private $tasks;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="lists")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Preference", inversedBy="list", cascade={"PERSIST", "REMOVE"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $preferences;

    /**
     * Конструктор класса TaskList
     */
    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return TaskList
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBackground(): ?string
    {
        return $this->background;
    }

    /**
     * @param string $background
     *
     * @return TaskList
     */
    public function setBackground(string $background): self
    {
        $this->background = $background;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBackgroundPath(): ?string
    {
        return $this->backgroundPath;
    }

    /**
     * @param string $backgroundPath
     *
     * @return TaskList
     */
    public function setBackgroundPath(string $backgroundPath): self
    {
        $this->backgroundPath = $backgroundPath;

        return $this;
    }

    /**
     * @param Task $task
     *
     * @return TaskList
     */
    public function addTask(Task $task): self
    {
        if (!$this->getTasks()->contains($task)) {
            $this->getTasks()->add($task);
            $task->setList($this);
        }

        return $this;
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    /**
     * @param Task $task
     *
     * @return TaskList
     */
    public function removeTask(Task $task): self
    {
        if ($this->getTasks()->contains($task)) {
            $this->getTasks()->removeElement($task);

            if ($task->getList() === $this) {
                $task->setList(null);
            }
        }

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     *
     * @return TaskList
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Preference|null
     */
    public function getPreferences(): ?Preference
    {
        return $this->preferences;
    }

    /**
     * @param Preference|null $preferences
     *
     * @return TaskList
     */
    public function setPreferences(?Preference $preferences): self
    {
        $this->preferences = $preferences;

        return $this;
    }
}
