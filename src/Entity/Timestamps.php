<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Трейт Timestamps
 *
 * @package App\Entity
 */
trait Timestamps
{
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist()
     */
    public function createdAt()
    {
        try {
            $this->createdAt = new \DateTime();
            $this->updatedAt = new \DateTime();
        } catch (\Exception $e) {
            $this->createdAt = null;
            $this->updatedAt = null;
        }
    }

    /**
     * @ORM\PreUpdate()
     */
    public function updateAt()
    {
        try {
            $this->updatedAt = new \DateTime();
        } catch (\Exception $e) {
            $this->updatedAt = null;
        }
    }
}