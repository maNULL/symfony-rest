<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Task
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;
    /**
     * @ORM\Column(type="boolean")
     */
    private $isComplete = false;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TaskList", inversedBy="tasks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $list;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Note", mappedBy="task", cascade={"remove"})
     */
    private $notes;

    /**
     * Конструктор класса Task
     */
    public function __construct()
    {
        $this->notes = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Task
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsComplete(): ?bool
    {
        return $this->isComplete;
    }

    /**
     * @param bool $isComplete
     *
     * @return Task
     */
    public function setIsComplete(bool $isComplete): self
    {
        $this->isComplete = $isComplete;

        return $this;
    }

    /**
     * @return TaskList|null
     */
    public function getList(): ?TaskList
    {
        return $this->list;
    }

    /**
     * @param TaskList|null $list
     *
     * @return Task
     */
    public function setList(?TaskList $list): self
    {
        $this->list = $list;

        return $this;
    }

    /**
     * @param Note $note
     *
     * @return Task
     */
    public function addNote(Note $note): self
    {
        if (!$this->getNotes()->contains($note)) {
            $this->getNotes()->add($note);
            $note->setTask($this);
        }

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    /**
     * @param Note $note
     *
     * @return Task
     */
    public function removeNote(Note $note): self
    {
        if ($this->getNotes()->contains($note)) {
            $this->getNotes()->removeElement($note);

            if ($note->getTask() === $this) {
                $note->setTask(null);
            }
        }

        return $this;
    }
}
