<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\TaskList;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * Класс PreferenceController
 *
 * @package App\Controller
 */
class PreferenceController extends AbstractFOSRestController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Конструктор класса PreferenceController
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param TaskList $list
     *
     * @return View
     */
    public function getPreferencesAction(TaskList $list): View
    {
        return $this->view($list->getPreferences(), Response::HTTP_OK);
    }

    /**
     * @Rest\RequestParam(name="sortValue", description="The value will be used to sort the list", nullable=false)
     * @param ParamFetcher $paramFetcher
     * @param TaskList     $list
     *
     * @return View
     */
    public function sortPreferencesAction(ParamFetcher $paramFetcher, TaskList $list): View
    {
        $sortValue = $paramFetcher->get('sortValue');

        if ($sortValue) {
            $list->getPreferences()->setSortValue($sortValue);

            $this->entityManager->persist($list);
            $this->entityManager->flush();

            return $this->view(null, Response::HTTP_NO_CONTENT);
        }

        return $this->view(['message' => 'The sort value cannot be null'], Response::HTTP_CONFLICT);
    }

    /**
     * @Rest\RequestParam(name="filterValue", description="The filter value", nullable=false)
     * @param ParamFetcher $paramFetcher
     * @param TaskList     $list
     *
     * @return View
     */
    public function filterPreferencesAction(ParamFetcher $paramFetcher, TaskList $list): View
    {
        $filterValue = $paramFetcher->get('filterValue');

        if ($filterValue) {
            $list->getPreferences()->setFilterValue($filterValue);

            $this->entityManager->persist($list);
            $this->entityManager->flush();

            return $this->view(null, Response::HTTP_NO_CONTENT);
        }

        return $this->view(['message' => 'The filter value cannot be null'], Response::HTTP_CONFLICT);
    }
}
