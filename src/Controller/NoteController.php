<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Note;
use App\Repository\NoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * Класс NoteController
 *
 * @package App\Controller
 */
class NoteController extends AbstractFOSRestController
{
    /**
     * @var NoteRepository
     */
    private $noteRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Конструктор класса NoteController
     *
     * @param NoteRepository         $noteRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(NoteRepository $noteRepository, EntityManagerInterface $entityManager)
    {

        $this->noteRepository = $noteRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Note $note
     *
     * @return View
     */
    public function getNoteAction(Note $note): View
    {
        return $this->view($note, Response::HTTP_OK);
    }

    /**
     * @param Note $note
     *
     * @return View
     */
    public function deleteNoteAction(Note $note): View
    {
        if ($note) {
            $this->entityManager->remove($note);
            $this->entityManager->flush();

            return $this->view(null, Response::HTTP_NO_CONTENT);
        }

        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
