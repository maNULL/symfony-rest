<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Preference;
use App\Entity\Task;
use App\Entity\TaskList;
use App\Repository\TaskListRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Класс ListController
 *
 * @package App\Controller
 */
class ListController extends AbstractFOSRestController
{
    /**
     * @var TaskListRepository
     */
    private $taskListRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Конструктор класса ListController
     *
     * @param TaskListRepository     $taskListRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(TaskListRepository $taskListRepository, EntityManagerInterface $entityManager)
    {

        $this->taskListRepository = $taskListRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @return View
     */
    public function getListsAction(): View
    {
        $data = $this->taskListRepository->findAll();

        return $this->view($data, Response::HTTP_OK);
    }

    /**
     * @param TaskList $list
     *
     * @return View
     */
    public function getListAction(TaskList $list): View
    {
        return $this->view($list, Response::HTTP_OK);
    }

    /**
     * @Rest\RequestParam(name="title", description="Title of the list", nullable=false)
     * @param ParamFetcher $paramFetcher
     *
     * @return View
     */
    public function postListsAction(ParamFetcher $paramFetcher): View
    {
        $title = $paramFetcher->get('title');

        if ($title) {
            $list = new TaskList();

            $preference = new Preference();

            $preference
                ->setSortValue('date')
                ->setFilterValue('remaining')
                ->addList($list);

            $list
                ->setUser($this->getUser())
                ->setPreferences($preference)
                ->setTitle($title);

            $this->entityManager->persist($list);
            $this->entityManager->flush();

            return $this->view($list, Response::HTTP_OK);
        }

        return $this->view(['title' => 'This can not be null'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\RequestParam(name="title", description="Title of the new task", nullable=false)
     * @param ParamFetcher $paramFetcher
     * @param TaskList     $list
     *
     * @return View
     */
    public function postListTaskAction(ParamFetcher $paramFetcher, TaskList $list): View
    {
        if ($list) {
            $title = $paramFetcher->get('title');

            $task = new Task();
            $task->setTitle($title)
                ->setList($list);

            $list->addTask($task);

            $this->entityManager->persist($task);
            $this->entityManager->flush();

            return $this->view($task, Response::HTTP_OK);
        }

        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param TaskList $list
     *
     * @return View
     */
    public function getListsTasksAction(TaskList $list): View
    {
        return $this->view($list->getTasks(), Response::HTTP_OK);
    }

    public function putListsAction()
    {

    }

    /**
     * @Rest\FileParam(name="image", description="the background if the list", nullable=false, image=true)
     * @param Request      $request
     * @param ParamFetcher $paramFetcher
     * @param TaskList     $list
     *
     * @return View
     */
    public function backgroundListsAction(Request $request, ParamFetcher $paramFetcher, TaskList $list): View
    {
        $currentBackground = $list->getBackground();

        /** @var UploadedFile $file */
        $file = $paramFetcher->get('image');

        //TODO: remove old file if any
        if (!is_null($currentBackground)) {
            $filesystem = new Filesystem();
            $filesystem->remove(
                $this->getUploadsDir().$currentBackground
            );
        }

        if ($file) {
            $filename = md5(uniqid()).'.'.$file->guessClientExtension();
            $file->move(
                $this->getUploadsDir(),
                $filename
            );

            $list->setBackground($filename)
                ->setBackgroundPath('/uploads/'.$filename);

            $this->entityManager->persist($list);
            $this->entityManager->flush();

            $data = $request->getUriForPath(
                $list->getBackgroundPath()
            );

            return $this->view($data, Response::HTTP_OK);
        }

        return $this->view(['message' => 'Something went wrong'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return string
     */
    private function getUploadsDir(): string
    {
        return $this->getParameter('uploads_dir');
    }

    /**
     * @param TaskList $list
     *
     * @return View
     */
    public function deleteListAction(TaskList $list): View
    {
        $this->entityManager->remove($list);
        $this->entityManager->flush();

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\RequestParam(name="title", description="The new title for the list", nullable=false)
     * @param ParamFetcher $paramFetcher
     * @param TaskList     $list
     *
     * @return View
     */
    public function titleListAction(ParamFetcher $paramFetcher, TaskList $list): View
    {
        $errors = [];
        $title = $paramFetcher->get('title');

        if ('' !== trim($title)) {
            if ($list) {
                $list->setTitle($title);

                $this->entityManager->persist($list);
                $this->entityManager->flush();

                return $this->view(null, Response::HTTP_NO_CONTENT);
            }

            $errors[] = ['title' => 'This value cannot be empty'];
        }

        $errors[] = ['list' => 'List not found'];

        return $this->view($errors, Response::HTTP_NO_CONTENT);
    }
}
