<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Note;
use App\Entity\Task;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * Класс TaskController
 *
 * @package App\Controller
 */
class TaskController extends AbstractFOSRestController
{
    /**
     * @var TaskRepository
     */
    private $taskRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Конструктор класса TaskController
     *
     * @param TaskRepository         $taskRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(TaskRepository $taskRepository, EntityManagerInterface $entityManager)
    {

        $this->taskRepository = $taskRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Task $task
     *
     * @return View
     */
    public function deleteTaskAction(Task $task): View
    {
        if ($task) {
            $this->entityManager->remove($task);
            $this->entityManager->flush();

            return $this->view(null, Response::HTTP_NO_CONTENT);
        }

        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Task $task
     *
     * @return View
     */
    public function getTaskAction(Task $task): View
    {
        return $this->view($task, Response::HTTP_OK);
    }

    /**
     * @param Task $task
     *
     * @return View
     */
    public function getTaskNotesAction(Task $task): View
    {
        if ($task) {
            return $this->view($task->getNotes(), Response::HTTP_OK);
        }

        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Task $task
     *
     * @return View
     */
    public function statusTaskAction(Task $task): View
    {
        if ($task) {
            $task->setIsComplete(!$task->getIsComplete());

            $this->entityManager->persist($task);
            $this->entityManager->flush();

            return $this->view($task->getIsComplete(), Response::HTTP_OK);
        }

        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @Rest\RequestParam(name="note", description="Note for the task", nullable=false)
     * @param ParamFetcher $paramFetcher
     * @param Task         $task
     *
     * @return View
     */
    public function postTaskNotesAction(ParamFetcher $paramFetcher, Task $task): View
    {
        $noteString = $paramFetcher->get('note');

        if ($noteString) {
            if ($task) {
                $note = new Note();
                $note->setNote($noteString)
                    ->setTask($task);

                $task->addNote($note);

                $this->entityManager->persist($note);
                $this->entityManager->flush();

                return $this->view($note, Response::HTTP_OK);
            }
        }

        return $this->view(['message' => 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
