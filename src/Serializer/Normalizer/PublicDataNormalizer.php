<?php
declare(strict_types=1);

namespace App\Serializer\Normalizer;

use App\Entity\Note;
use App\Entity\Task;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Класс PublicDataNormalizer
 *
 * @package App\Serializer\Normalizer
 */
class PublicDataNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    /**
     * Конструктор класса PublicDataNormalizer
     *
     * @param ObjectNormalizer $objectNormalizer
     */
    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    /**
     * @param mixed $object
     * @param null  $format
     * @param array $context
     *
     * @return array|bool|float|int|string
     * @throws ExceptionInterface
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $context['ignored_attributes'] = ['user'];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    /**
     * @param mixed $data
     * @param null  $format
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Task || $data instanceof Note;
    }
}