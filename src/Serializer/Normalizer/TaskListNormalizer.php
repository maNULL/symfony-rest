<?php
declare(strict_types=1);

namespace App\Serializer\Normalizer;

use App\Entity\TaskList;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Класс TaskListNormalizer
 *
 * @package App\Serializer\Normalizer
 */
class TaskListNormalizer implements NormalizerInterface
{
    /**
     * @var Packages
     */
    private $packages;
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    /**
     * Конструктор класса TaskListNormalizer
     *
     * @param ObjectNormalizer $objectNormalizer
     * @param Packages         $packages
     */
    public function __construct(ObjectNormalizer $objectNormalizer, Packages $packages)
    {
        $this->packages = $packages;
        $this->objectNormalizer = $objectNormalizer;
    }

    /**
     * @param mixed $object
     * @param null  $format
     * @param array $context
     *
     * @return array|bool|float|int|mixed|string
     * @throws ExceptionInterface
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $object->setBackgroundPath(
            $this->packages->getUrl($object->getBackgroundPath(), 'backgrounds')
        );

        $context['ignored_attributes'] = ['user'];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    /**
     * @param mixed $data
     * @param null  $format
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof TaskList;
    }
}