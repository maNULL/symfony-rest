<?php
declare(strict_types=1);

namespace App\Listeners;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Класс AuthenticationSuccessListener
 *
 * @package App\Listeners
 */
class AuthenticationSuccessListener
{
    private $secure = false;
    /**
     * @var string
     */
    private $tokenTTL;

    /**
     * Конструктор класса AuthenticationSuccessListener
     *
     * @param string $tokenTTL
     */
    public function __construct(string $tokenTTL)
    {

        $this->tokenTTL = $tokenTTL;
    }

    /**
     * @param AuthenticationSuccessEvent $event
     *
     * @throws \Exception
     */
    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        $response = $event->getResponse();
        $data = $event->getData();

        $token = $data['token'];
        unset($data['token']);
        $event->setData($data);

        $response->headers->setCookie(
            new Cookie(
                'BEARER',
                $token,
                (new \DateTime())
                    ->add(new \DateInterval('PT'.$this->tokenTTL.'S'))
                , '/', null, $this->secure
            )
        );
    }
}