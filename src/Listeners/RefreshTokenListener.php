<?php
declare(strict_types=1);

namespace App\Listeners;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Класс RefreshTokenListener
 *
 * @package App\Listeners
 */
class RefreshTokenListener implements EventSubscriberInterface
{
    private $secure = false;
    /**
     * @var string
     */
    private $ttl;

    /**
     * Конструктор класса AuthenticationSuccessListener
     *
     * @param string $ttl
     */
    public function __construct(string $ttl)
    {

        $this->ttl = $ttl;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'lexik_jwt_authentication.on_authentication_success' => [
                ['setRefreshToken'],
            ],
        ];
    }

    /**
     * @param AuthenticationSuccessEvent $event
     *
     * @throws \Exception
     */
    public function setRefreshToken(AuthenticationSuccessEvent $event)
    {
        $refreshToken = $event->getData()['refresh_token'];
        $response = $event->getResponse();

        if ($refreshToken) {
            $response->headers->setCookie(
                new Cookie(
                    'REFRESH_TOKEN',
                    $refreshToken,
                    (new \DateTime())
                        ->add(new \DateInterval('PT'.$this->ttl.'S'))
                    , '/', null, $this->secure
                )
            );
        }
    }
}